# Update gcloud
gcloud components update

# Update gcloud kubectl
gcloud components update kubectl

# Echo credentials of gitlab-deployer to a local file
echo -n $GCP_SECRET_ACCESS_KEY > 'service-account.json'

# Authenticate via this file
gcloud auth activate-service-account --key-file 'service-account.json'

# Delete local copy of cred file
shred -zu service-account.json

# Set project
gcloud config set project wide-brook-240012

# Set Region and Zone
gcloud config set compute/zone europe-west2-a

# Retrieve creds to authenticate to k8s cluster
gcloud container clusters get-credentials multi-cluster

# Apply K8 config files
kubectl apply -f ./k8s

# Deploy all docker images
kubectl set image deployment/server-deployment api=$VERSION_API_XYZ
kubectl set image deployment/client-deployment client=$VERSION_CLIENT_XYZ
kubectl set image deployment/worker-deployment worker=$VERSION_WORKER_XYZ